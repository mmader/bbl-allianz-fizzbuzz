import java.util.stream.IntStream;

public class Program {
    public static void main(String[] args) {
        IntStream.range(1, 101).forEach(x -> {
            System.out.println(compute(x));
        });
    }

    static String compute(int x) {
        if(x % 3 == 0) {
            if(x % 5 == 0) {
                return "FizzBuzz";
            }
            else {
                return "Fizz";
            }
        } else if(x % 5 == 0) {
            return "Buzz";
        } else {
            return String.valueOf(x);
        }
    }
}
