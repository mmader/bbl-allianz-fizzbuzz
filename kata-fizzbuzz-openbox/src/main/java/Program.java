import com.kawabytes.kata.DefaultFizzBuzzAlgorithm;
import com.kawabytes.kata.FizzBuzzKata;

import java.io.IOException;
import java.io.PrintStream;

public class Program {
    public static void main(String[] args) throws IOException {
        final var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();
        final var printStream = new PrintStream(System.out);

        final var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithm, printStream);

        fizzBuzzKata.printFizzBuzz(1, 100);
    }
}
