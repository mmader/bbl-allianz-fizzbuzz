package com.kawabytes.kata;

import net.jqwik.api.*;
import net.jqwik.api.arbitraries.IntegerArbitrary;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.verification.VerificationMode;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class FizzBuzzKataTest {
    private static final String ARGUMENT_OUT_OF_RANGE_PROVIDER
            = "argumentOutOfRangeProvider";

    private static final String ARGUMENT_IN_RANGE_PROVIDER
            = "argumentInRangeProvider";

    @Mock
    private PrintStream printStream;

    @Mock
    private FizzBuzzAlgorithm fizzBuzzAlgorithm;

    @Property
    void given_out_of_range_start_value_should_throw_illegal_argument_exception(
            @ForAll(ARGUMENT_OUT_OF_RANGE_PROVIDER) int outOfRangeValue)
    {
        // 1. Arrange
        MockitoAnnotations.initMocks(this);
        final var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithm, printStream);

        // 3. Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            // 2. Act
            fizzBuzzKata.printFizzBuzz(outOfRangeValue, 100);
        });

        verifyNoInteractions(fizzBuzzAlgorithm);
        verifyNoInteractions(printStream);
    }

    @Property
    void given_out_of_range_count_value_should_throw_illegal_argument_exception(
            @ForAll(ARGUMENT_OUT_OF_RANGE_PROVIDER) int outOfRangeValue)
    {
        // 1. Arrange
        MockitoAnnotations.initMocks(this);
        final var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithm, printStream);

        // 3. Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            // 2. Act
            fizzBuzzKata.printFizzBuzz(1, outOfRangeValue);
        });

        verifyNoInteractions(fizzBuzzAlgorithm);
        verifyNoInteractions(printStream);
    }

    @Provide
    private IntegerArbitrary argumentOutOfRangeProvider() {
        return Arbitraries.integers().lessOrEqual(0);
    }

    @Property(tries = 10)
    void should_call_compute_fizzbuzz_in_order_and_pass_its_return_value_to_the_print_stream(
            @ForAll(ARGUMENT_IN_RANGE_PROVIDER) int start,
            @ForAll(ARGUMENT_IN_RANGE_PROVIDER) int count)
    {
        // 1. Arrange
        MockitoAnnotations.initMocks(this);
        final var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithm, printStream);

        when(fizzBuzzAlgorithm.compute(Mockito.anyInt())).thenAnswer(x -> x.getArgument(0).toString());

        // 2. Act
        fizzBuzzKata.printFizzBuzz(start, count);

        // 3. Assert
        final var fizzBuzzAlgorithmInOrderVerifier = Mockito.inOrder(fizzBuzzAlgorithm);
        final var printStreamInOrderVerifier = Mockito.inOrder(printStream);

        for(var i = start; i < start + count; ++i) {
            fizzBuzzAlgorithmInOrderVerifier.verify(fizzBuzzAlgorithm, times(1)).compute(i);
            printStreamInOrderVerifier.verify(printStream, times(1)).println(String.valueOf(i));
        }

        verifyNoMoreInteractions(fizzBuzzAlgorithm);
        verifyNoMoreInteractions(printStream);
    }

    @Provide
    private IntegerArbitrary argumentInRangeProvider() {
        return Arbitraries.integers().between(1, 100);
    }
}

